package co.simplon.promo16.bases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

public class LoopingTest {
    @Test
    public void testAverageChars() {
        Looping testLooping = new Looping();
        assertEquals(4, testLooping.averageChars("bloup blip blop blap"));
    }

    @Test
    public void testFindTrue() {
        Looping testLooping = new Looping();
        String[] words = { "test", "test1", "test2" };
        assertTrue(testLooping.find(words, "test"));

    }

    @Test
    public void testFindMax() {
        Looping testLooping = new Looping();
        assertEquals(10, testLooping.findMax(List.of(1, 5, 8, 10)));

    }
}
