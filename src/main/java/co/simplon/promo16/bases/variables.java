package co.simplon.promo16.bases;

import java.util.ArrayList;
import java.util.List;

public class variables {
    public void first() {
        int age = 23;
        String promo = "P16";
        boolean iLoveJava = true;
        List<String> languages = new ArrayList<>(List.of("java", "css", "html", "c++", "php"));
        System.out.println(age + promo + iLoveJava + languages);
    }

    public void withParameters(String string, int x) {
        System.out.println(string + x);
    }

    public String withReturn() {
        return "le return";
    }
}
