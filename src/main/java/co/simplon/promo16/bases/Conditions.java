package co.simplon.promo16.bases;

public class Conditions {
    public boolean isPositive(int x) {
        if (x < 0) {
            return false;
        } else
            return true;
    }

    public void skynetIA(String phrase) {
        if (phrase == "Hello") {
            System.out.println("Hello how are you ?");
        } else if (phrase == "How are you ?") {
            System.out.println("I'm fine, thank you");
        } else if (phrase == "Goodbye") {
            System.out.println("Goodbye friend");
        } else {
            System.out.println("I don't understand");
        }

    }

    public void buy(int age, String produit) {
        if (produit == "Alcohol" && age < 18) {
            System.out.println("tu es trop jeune");
        } else {
            System.out.println("c'est ok");
        }
    }

    public void greater(int x, int y) {
        if (x > y) {
            System.out.println(x);
        } else {
            System.out.println(y);
        }

    }
}
