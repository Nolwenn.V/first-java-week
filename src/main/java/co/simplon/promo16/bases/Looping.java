package co.simplon.promo16.bases;

import java.util.List;

public class Looping {
    public void boucle(String mots, int y) {
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print(mots + " ");
            }
            System.out.println(mots);
        }

    }

    public int findMax(List<Integer> list) {
        int x = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > x) {
                x = list.get(i);

            }

        }
        return x;

    }

    public boolean find(String[] tableau, String mot) {
        for (int i = 0; i < tableau.length; i++) {
            if (mot == tableau[i]) {
                return true;

            }

        }
        return false;

    }

    public int averageChars(String sentence) {
        String[] wordArray = sentence.split(" ");
        System.out.println(wordArray.length);
        int lettersCount = 0;
        for (String word : wordArray) {
            lettersCount += word.length();
        }
        return lettersCount / wordArray.length;
    }

}
