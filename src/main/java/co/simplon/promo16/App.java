package co.simplon.promo16;

import java.util.ArrayList;
import java.util.List;

import co.simplon.promo16.bases.Conditions;
import co.simplon.promo16.bases.Looping;
import co.simplon.promo16.bases.variables;
import co.simplon.promo16.oop.Laptop;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        variables variables = new variables();
        variables.first();
        System.out.println(variables.withReturn());

        Conditions condition = new Conditions();
        System.out.println(condition.isPositive(-12));

        Laptop laptop = new Laptop("lenovo", 100, true, true);
        laptop.plug();
        laptop.powerSwitch();
        laptop.instal("windows");

        Looping looping = new Looping();
        looping.boucle("bloup", 12);
        List<Integer> list = new ArrayList<>(List.of(28, 12, 1, 45));
        looping.findMax(list);
        String[] nourriture = new String[] { "banane", "chocolat", "fraise" };
        System.out.println(looping.find(nourriture, "banane"));

    }

}
