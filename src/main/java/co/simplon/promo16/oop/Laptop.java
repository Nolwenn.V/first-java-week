package co.simplon.promo16.oop;

public class Laptop {
    String model;
    int battery = 50;
    boolean turnON;
    boolean pluggedin;
    String OS = "null";

    public Laptop(String model, int battery, boolean turnON, boolean pluggedin) {
        this.model = model;
        this.battery = battery;
        this.turnON = turnON;
        this.pluggedin = pluggedin;
    }

    public void plug() {
        pluggedin = true;
        consumeBattery();

    }

    public void powerSwitch() {
        if (turnON == true) {
            turnON = false;
        } else if (turnON == false && pluggedin == true) {
            turnON = true;
        } else if (turnON == false && pluggedin == false && battery > 10) {
            turnON = true;
            consumeBattery();

        } else {
            System.out.println("l'ordi ne démarre pas");
        }
    }

    public void instal(String OS) {
        if (turnON == true) {
            System.out.println("instalation en cours...");

        } else if (pluggedin == false) {
            consumeBattery();

        }

    }

    private void consumeBattery() {
        if (pluggedin == false) {
            battery -= 5;

        } else if (pluggedin == true) {
            battery += 5;

        } else if (battery == 0) {
            turnON = false;

        } else if (pluggedin == true) {

        } else if (battery >= 100) {
            battery = 100;

        }

    }
}
